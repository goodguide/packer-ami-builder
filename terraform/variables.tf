variable "name" {
  default="packer"
}

variable "region" {
  default = "us-east-1"
}

variable "subnet_bitsize" {
  default = 8
}

variable "cidr" {
  default = "10.0.0.0/16"
}
